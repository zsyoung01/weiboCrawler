package com.yk.weibo.servie.build.api;

import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * Project Name:weiboCrawler
 * File Name:IUrlBuildService
 * Package Name:com.yk.weibo.servie.build.api
 * Date:2018/1/9 15:34
 * Author:zhangshaoyang
 * Description:
 * Copyright (c) 2018, 重庆云凯科技有限公司 All Rights Reserved.
 */
public interface IUrlBuildService {

//    List<URL> buildUserIdUrl(List<String> idList);
//
//    URL buildUserIdUrl(String id);

//    Map<String, String> acquireDomain (String id);

    /**
     * 获取微博总页数
     *
     * @param id
     * @param cookie
     * @return
     */
    int acquireCountPage(String id, String cookie);


    /**
     * 获取domain和domain_op
     *
     * @param id
     * @param cookie
     * @return
     */
    String acquireDomain(String id, String cookie);


    /**
     * 构建微博内容页链接
     *
     * @param id
     * @param cookie
     * @param time
     * @param prePage
     * @return
     */
    List<URL> buildContentUrl(String id, String cookie, long time, int prePage);


}

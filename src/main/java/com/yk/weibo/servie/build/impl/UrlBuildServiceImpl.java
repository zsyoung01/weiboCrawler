package com.yk.weibo.servie.build.impl;

import com.yk.weibo.servie.build.api.IUrlBuildService;
import com.yk.weibo.servie.content.api.IContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Project Name:weiboCrawler
 * File Name:UrlBuildServiceImpl
 * Package Name:com.yk.weibo.servie.build.impl
 * Date:2018/1/9 15:37
 * Author:zhangshaoyang
 * Description:
 * Copyright (c) 2018, 重庆云凯科技有限公司 All Rights Reserved.
 */
@Service("urlBuildService")
public class UrlBuildServiceImpl implements IUrlBuildService {

    @Autowired
    private IContentService contentService;
    private static String userHomePageBaseUrl = "https://weibo.com/u/";
    private static String userHomePageBaseUrlTail = "?refer_flag=1001030201_&is_all=1";
    private static String contentUrlBaseUrl = "https://weibo.com/p/aj/v6/mblog/mbloglist?";


    /**
     * 获取微博总页数
     *
     * @param id
     * @param cookie
     * @return
     */
    @Override
    public int acquireCountPage(String id, String cookie) {
        long time = System.currentTimeMillis();
        URL contentUrl = this.composeURLTopHalf(contentUrlBaseUrl, id, cookie, time, 1);
        System.out.println("contentUrl:" + contentUrl);
        int countPage = 0;
        try {
            StringBuffer buffer = contentService.getContent(cookie, contentUrl);
            Pattern pattern = Pattern.compile("&countPage=\\d*");
            Matcher matcher = pattern.matcher(buffer.toString());
            while (matcher.find()) {
                //获取字符串
                StringTokenizer stringTokenizer = new StringTokenizer(matcher.group(), "=");
                stringTokenizer.nextToken();
                //取得域名
                countPage = Integer.parseInt(stringTokenizer.nextToken());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return countPage;
    }

    /**
     * 获取domain和domain_op
     *
     * @param id
     * @param cookie
     * @return
     */
    @Override
    public String acquireDomain(String id, String cookie) {
        String userHomePageUrl = userHomePageBaseUrl + id + userHomePageBaseUrlTail;
        String domain = "";
        try {
            URL url = new URL(userHomePageUrl);
            StringBuffer buffer = contentService.getContent(cookie, url);
            Pattern pattern = Pattern.compile("(\\$CONFIG\\['domain'\\]='\\d{6}')");
            Matcher matcher = pattern.matcher(buffer.toString());
            while (matcher.find()) {
                //获取字符串
                StringTokenizer stringTokenizer = new StringTokenizer(matcher.group(), "=");
                stringTokenizer.nextToken();
                //取得域名
                domain = stringTokenizer.nextToken().replaceAll("'", "");
                System.out.println("domain:" + domain);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return domain;
    }




    /**
     * 构建微博内容页链接
     *
     * @param id
     * @param cookie
     * @param time
     * @param prePage
     * @return
     */
    @Override
    public List<URL> buildContentUrl(String id, String cookie, long time, int prePage) {
        List<URL> urls = new ArrayList<>();
        urls.add(this.composeURLTopHalf(contentUrlBaseUrl, id, cookie, time, prePage));
        urls.add(this.composeURLLowHalf(contentUrlBaseUrl, id, cookie, time, prePage));
        return urls;
    }

    /**
     * 组装链接0，页面上半部分
     *
     * @param baseUrl
     * @param id
     * @param cookie
     * @param time
     * @param prePage
     * @return
     */
    private URL composeURLTopHalf(String baseUrl, String id, String cookie, long time, int prePage) {
        String domain = acquireDomain(id, cookie);
        URL url = null;
        try {
            url = new URL(baseUrl +
                    "ajwvr=6&domain=" + domain + "&topnav=1" +
                    "&wvr=6" +
                    "&topsug=1" +
                    "&is_all=1" +
                    "&pagebar=1" +
                    "&pl_name=Pl_Official_MyProfileFeed__22" +
                    "&id=" + domain + id +
                    "&script_uri=/chinasolution" +
                    "&feed_type=0" +
                    "&page=" + prePage +
                    "&pre_page=" + prePage +
                    "&domain_op=" + domain +
                    "&_rnd=" + time);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url;
    }

    /**
     * 组装链接1，页面下半部分
     *
     * @param baseUrl
     * @param id
     * @param cookie
     * @param time
     * @param prePage
     * @return
     */
    private URL composeURLLowHalf(String baseUrl, String id, String cookie, long time, int prePage) {
        String domain = acquireDomain(id, cookie);
        URL url = null;
        try {
            url = new URL(baseUrl +
                    "ajwvr=6&domain=" + domain + "&topnav=1" +
                    "&wvr=6" +
                    "&topsug=1" +
                    "&is_all=1" +
                    "&pagebar=0" +
                    "&pl_name=Pl_Official_MyProfileFeed__22" +
                    "&id=" + domain + id +
                    "&script_uri=/chinasolution" +
                    "&feed_type=0" +
                    "&page=" + prePage +
                    "&pre_page=" + prePage +
                    "&domain_op=" + domain +
                    "&_rnd=" + time);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url;
    }


}

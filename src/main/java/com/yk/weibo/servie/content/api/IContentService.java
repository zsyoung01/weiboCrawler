package com.yk.weibo.servie.content.api;

import com.yk.weibo.model.SinaModel;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * Project Name:weiboCrawler
 * File Name:IContentService
 * Package Name:com.yk.weibo.servie.content.api
 * Date:2018/1/9 18:05
 * Author:zhangshaoyang
 * Description:
 * Copyright (c) 2018, 重庆云凯科技有限公司 All Rights Reserved.
 */
public interface IContentService {

    /**
     * 获取网页内容
     *
     * @param cookie
     * @param url
     * @return
     * @throws IOException
     */
    StringBuffer getContent(String cookie, URL url) throws IOException;


    /**
     * 解析网页内容
     *
     * @param html
     * @return
     */
    List<SinaModel> analyzeContent(String html);

    /**
     * 解析网页元素
     *
     * @param e
     * @return
     */
    SinaModel analyzeSingleContent(Element e);

}

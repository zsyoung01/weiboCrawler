package com.yk.weibo.dao.common.api;

import org.hibernate.Session;

import java.util.HashSet;
import java.util.List;

/**
 * Created by Satani on 2017/5/17.
 */
public interface ICommonDAO {
    /**
     * 获取session
     *
     * @return session
     */
    Session getSession();
    /**
     * 添加
     * @param object 实体对象
     * @return 添加状态，true：成功，false：失败
     */
    boolean save(Object object);

    boolean saveObjects(List<Object> objects);

    /**
     * 删除
     * @param object 实体对象
     * @return 删除状态，true：成功，false：失败
     */
    boolean delete(Object object);

    /**
     * 更新
     * @param object 实体对象
     * @return 更新状态，true：成功，false：失败
     */
    boolean update(Object object);

    /**
     * 获取与综合条件
     * @param conditions 条件队列
     * @return 综合条件
     */
    String getAndCondition(HashSet<String> conditions);

    /**
     * 获取或综合条件
     * @param conditions 条件队列
     * @return 综合条件
     */
    String getOrCondition(HashSet<String> conditions);
}

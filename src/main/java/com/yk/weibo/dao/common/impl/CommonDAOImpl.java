package com.yk.weibo.dao.common.impl;


import com.yk.weibo.dao.common.api.ICommonDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Satani on 2017/5/17.
 */
@Repository("commonDAO")
public class CommonDAOImpl implements ICommonDAO {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * 获取session
     *
     * @return session
     */
    @Override
    public Session getSession() {
        return this.sessionFactory.openSession();
    }

    /**
     * 添加
     *
     * @param object 实体对象
     * @return 添加状态，true：成功，false：失败
     */
    @Override
    public boolean save(Object object) {
        boolean state=false;
        Session session=this.getSession();
        try{
            session.beginTransaction();
            session.save(object);
            session.getTransaction().commit();
            state=true;
        }catch (Exception e){
            //exception log
            System.out.println("here");
        }
        finally {
            session.close();
        }

        return state;
    }

    @Override
    public boolean saveObjects(List objects) {
        boolean state=false;
        Session session=this.getSession();
        try{
            session.beginTransaction();
            for(Object object:objects){
                session.save(object);
            }
            session.getTransaction().commit();
            state=true;
        }catch (Exception e){
            //exception log
            System.out.println("here");
        }
        finally {
            session.close();
        }

        return state;
    }

    /**
     * 删除
     *
     * @param object 实体对象
     * @return 删除状态，true：成功，false：失败
     */
    @Override
    public boolean delete(Object object) {
        boolean state=false;
        try{
            Session session=this.getSession();
            session.beginTransaction();
            session.delete(object);
            session.getTransaction().commit();
            session.close();
            state=true;
        }catch (Exception e){
            //exception log
        }

        return state;
    }

    /**
     * 更新
     *
     * @param object 实体对象
     * @return 更新状态，true：成功，false：失败
     */
    @Override
    public boolean update(Object object) {
        boolean state=false;
        try{
            Session session=this.getSession();
            session.beginTransaction();
            session.update(object);
            session.getTransaction().commit();
            session.close();
            state=true;
        }catch (Exception e){
            //exception log
        }

        return state;
    }

    /**
     * 获取与综合条件
     *
     * @param conditions 条件队列
     * @return 综合条件
     */
    @Override
    public String getAndCondition(HashSet<String> conditions) {
        String oneCondition="";
        if(conditions.isEmpty())
            return oneCondition;
        Iterator<String> iterator=conditions.iterator();
        while (iterator.hasNext()){
            String condition=iterator.next();
            if(!condition.isEmpty()){
                oneCondition+=condition;
                if(iterator.hasNext())
                    oneCondition+=" and ";
            }
        }
        if(conditions.size()>1)
            oneCondition="("+oneCondition+")";
        return oneCondition;
    }

    /**
     * 获取或综合条件
     *
     * @param conditions 条件队列
     * @return 综合条件
     */
    @Override
    public String getOrCondition(HashSet<String> conditions) {
        String oneCondition="";
        if(conditions.isEmpty())
            return oneCondition;
        Iterator<String> iterator=conditions.iterator();
        while (iterator.hasNext()){
            String condition=iterator.next();
            if(!condition.isEmpty()){
                oneCondition+=condition;
                if(iterator.hasNext())
                    oneCondition+=" or ";
            }
        }
        if(conditions.size()>1)
            oneCondition="("+oneCondition+")";
        return oneCondition;
    }
}

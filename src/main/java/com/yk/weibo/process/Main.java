package com.yk.weibo.process;


import com.sun.org.apache.regexp.internal.RE;
import com.yk.weibo.dao.RedisClient;
import com.yk.weibo.dao.common.api.ICommonDAO;
import com.yk.weibo.model.SinaModel;
import com.yk.weibo.servie.build.api.IUrlBuildService;
import com.yk.weibo.servie.content.api.IContentService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;


/**
 * Created by Satani on 2017/5/17.
 */
@Component("main")
public class Main {
    @Autowired
    private IUrlBuildService urlBuildService;
    @Autowired
    private IContentService contentService;
    @Autowired
    private ICommonDAO commonDAO;

    //点进需要爬的微博主页，链接中的10位数字即为id,如：https://weibo.com/u/5325190359?nick=Piano%E5%86%AF%E9%87%8C&is_all=1,其中5325190359为id
    private static String[] idList = {"3815389726"};
    //cookie需要自己去替换，在微博随便打开一个网页，F12->Network->All->Name中点击链接->Headers->Request Headers->Cookie
    private static String cookie = "SUB=_2AkMtCnOcf8NxqwJRmPEQzW3maoR_zwrEieKbVoJHJRMxHRl-yT9jqkgztRB6Bopdc3dlgEob1JEosw0rVeqIwmdjN6_a; SUBP=0033WrSXqPxfM72-Ws9jqgMF55529P9D9WWrxkRKCE7R8dQESNUQLRpq; SINAGLOBAL=9877439588234.533.1515652569790; TC-V5-G0=589da022062e21d675f389ce54f2eae7; _s_tentry=www.google.com.hk; UOR=,,www.google.com.hk; Apache=5138976855511.832.1515728692563; ULV=1515728692570:2:2:2:5138976855511.832.1515728692563:1515652569934; TC-Page-G0=1e758cd0025b6b0d876f76c087f85f2c; login_sid_t=f2eed256a3bf7447dbfa165b8fe3d359; cross_origin_proto=SSL; TC-Ugrow-G0=968b70b7bcdc28ac97c8130dd353b55e; wb_cusLike_3655689037=N";
    private static long time = System.currentTimeMillis();
    //ip和端口号在http://www.xicidaili.com/中找并替换
    public static String ip = "101.205.82.80";
    public static String port = "808";

    public void process() throws Exception {
        int countPage = 10000;
        //1.获取id，根据id获取domain
        for (String id : idList) {
            //2.获取总页数
            countPage = urlBuildService.acquireCountPage(id, cookie);
            System.out.println("countPage:" + countPage);

            //3.拼接链接
            List<URL> urls;
            for (int i = 1; i <= countPage; i++) {
                urls = urlBuildService.buildContentUrl(id, cookie, time, i);
                System.out.println(urls);
                //4.解析数据
                for (URL url : urls) {
                    StringBuffer str = contentService.getContent(cookie, url);
                    JSONObject json = new JSONObject(str.toString());
                    System.out.println(json);
                    String data = json.getString("data");
                    List<SinaModel> sinaModels = contentService.analyzeContent(data);
                    for (int j = 0; j < sinaModels.size(); j++) {
                        SinaModel sinaModel = sinaModels.get(j);
                        sinaModel.setUserId(id);
                        sinaModel.setUpdateTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));

                        boolean resultCache = RedisClient.set("sina" + j, sinaModel);
                        if (resultCache) {
                            SinaModel sinaModel1 = RedisClient.get("sina" + j, SinaModel.class);
                            if (sinaModel1 != null) {
                                System.out.println("第" + i + "页，第" + j + "个：" + sinaModel1.getUsername() + ":\n" + sinaModel1.getContent());
                            }
                            // System.out.println("向缓存中保存对象成功。");
                        } else {
                            System.out.println("向缓存中保存对象失败。");
                        }

                        //5.存入数据库
                        // this.commonDAO.save(sinaModel);
                        //System.out.println(sinaModel.toString());
                    }
                }

            }

        }

    }
}


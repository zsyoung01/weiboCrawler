package com.yk.weibo.process;

import redis.clients.jedis.Jedis;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Project Name:weiboCrawler
 * File Name:RedisJava
 * Package Name:com.yk.weibo.process
 * Date:2018/1/16 15:19
 * Author:zhangshaoyang
 * Description:
 * Copyright (c) 2018, 重庆云凯科技有限公司 All Rights Reserved.
 */
public class RedisJava {
    public static void main(String[] args) {
        //1.连接本地的Redis服务
        Jedis jedis = new Jedis("localhost");
        System.out.println("连接成功");

        //2.查看服务是否运行
        System.out.println("服务正在运行：" + jedis.ping());

        //3.1设置redis字符串数据
        jedis.set("myKey", "yunkai");
        //3.2 获取存储的数据并输出
        System.out.println("redis 存储的字符串为: " + jedis.get("key"));

        //4.1存储数据到列表中
        jedis.lpush("site-list", "alibaba");
        jedis.lpush("site-list", "taobao");
        jedis.lpush("site-list", "tencent");
        //4.2获取存储的数据并输出
        List<String> list = jedis.lrange("site-list", 0, 2);
        for (int i = 0; i < list.size(); i++) {
            System.out.println("列表项为：" + list.get(i));
        }

        //5.获取key并输出
        Set<String> keys = jedis.keys("*");
        Iterator<String> it = keys.iterator();
        while (it.hasNext()) {
            String key = it.next();
            System.out.println(key);
        }
    }
}

package com.yk.weibo.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Project Name:weiboCrawler
 * File Name:SinaModel
 * Package Name:com.yk.weibo.model
 * Date:2018/1/11 15:56
 * Author:zhangshaoyang
 * Description:
 * Copyright (c) 2018, 重庆云凯科技有限公司 All Rights Reserved.
 */
@Entity
@Table(name = "sina", schema = "weibo", catalog = "")
public class SinaModel {
    private String id;
    private String userId;
    private String content;
    private String contentlink;
    private String imglink;
    private Integer transmitnum;
    private Integer commentnum;
    private Integer likenum;
    private String username;
    private Timestamp publishTime;
    private String equipment;
    private Timestamp updateTime;

    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid2")
    @Id
    @Column(name = "id", nullable = false, length = 200)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "userId", nullable = true, length = 11)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "content", nullable = true, length = -1)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "contentlink", nullable = true, length = -1)
    public String getContentlink() {
        return contentlink;
    }

    public void setContentlink(String contentlink) {
        this.contentlink = contentlink;
    }

    @Basic
    @Column(name = "imglink", nullable = true, length = -1)
    public String getImglink() {
        return imglink;
    }

    public void setImglink(String imglink) {
        this.imglink = imglink;
    }

    @Basic
    @Column(name = "transmitnum", nullable = true)
    public Integer getTransmitnum() {
        return transmitnum;
    }

    public void setTransmitnum(Integer transmitnum) {
        this.transmitnum = transmitnum;
    }

    @Basic
    @Column(name = "commentnum", nullable = true)
    public Integer getCommentnum() {
        return commentnum;
    }

    public void setCommentnum(Integer commentnum) {
        this.commentnum = commentnum;
    }

    @Basic
    @Column(name = "likenum", nullable = true)
    public Integer getLikenum() {
        return likenum;
    }

    public void setLikenum(Integer likenum) {
        this.likenum = likenum;
    }

    @Basic
    @Column(name = "username", nullable = true, length = 200)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "publishTime", nullable = true)
    public Timestamp getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Timestamp publishTime) {
        this.publishTime = publishTime;
    }

    @Basic
    @Column(name = "equipment", nullable = true, length = 200)
    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    @Basic
    @Column(name = "updateTime", nullable = true)
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SinaModel sinaModel = (SinaModel) o;

        if (id != null ? !id.equals(sinaModel.id) : sinaModel.id != null) return false;
        if (userId != null ? !userId.equals(sinaModel.userId) : sinaModel.userId != null) return false;
        if (content != null ? !content.equals(sinaModel.content) : sinaModel.content != null) return false;
        if (contentlink != null ? !contentlink.equals(sinaModel.contentlink) : sinaModel.contentlink != null)
            return false;
        if (imglink != null ? !imglink.equals(sinaModel.imglink) : sinaModel.imglink != null) return false;
        if (transmitnum != null ? !transmitnum.equals(sinaModel.transmitnum) : sinaModel.transmitnum != null)
            return false;
        if (commentnum != null ? !commentnum.equals(sinaModel.commentnum) : sinaModel.commentnum != null) return false;
        if (likenum != null ? !likenum.equals(sinaModel.likenum) : sinaModel.likenum != null) return false;
        if (username != null ? !username.equals(sinaModel.username) : sinaModel.username != null) return false;
        if (publishTime != null ? !publishTime.equals(sinaModel.publishTime) : sinaModel.publishTime != null)
            return false;
        if (equipment != null ? !equipment.equals(sinaModel.equipment) : sinaModel.equipment != null) return false;
        if (updateTime != null ? !updateTime.equals(sinaModel.updateTime) : sinaModel.updateTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (contentlink != null ? contentlink.hashCode() : 0);
        result = 31 * result + (imglink != null ? imglink.hashCode() : 0);
        result = 31 * result + (transmitnum != null ? transmitnum.hashCode() : 0);
        result = 31 * result + (commentnum != null ? commentnum.hashCode() : 0);
        result = 31 * result + (likenum != null ? likenum.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (publishTime != null ? publishTime.hashCode() : 0);
        result = 31 * result + (equipment != null ? equipment.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        return result;
    }
}

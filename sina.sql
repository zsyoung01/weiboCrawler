/*
Navicat MySQL Data Transfer

Source Server         : 10.0.104.50
Source Server Version : 50720
Source Host           : 10.0.104.50:3306
Source Database       : weibo

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2018-01-12 18:19:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sina
-- ----------------------------
DROP TABLE IF EXISTS `sina`;
CREATE TABLE `sina` (
  `id` varchar(200) NOT NULL COMMENT '表id',
  `userId` varchar(11) DEFAULT NULL COMMENT '用户Id',
  `content` longtext COMMENT '微博正文',
  `contentlink` longtext COMMENT '微博内容中的链接',
  `imglink` longtext COMMENT '图片链接',
  `transmitnum` int(11) DEFAULT NULL COMMENT '转发数',
  `commentnum` int(11) DEFAULT NULL COMMENT '评论数',
  `likenum` int(11) DEFAULT NULL COMMENT '点赞数',
  `username` varchar(200) DEFAULT NULL COMMENT '用户名',
  `publishTime` datetime DEFAULT NULL COMMENT '发布时间',
  `equipment` varchar(200) DEFAULT NULL COMMENT '设备',
  `updateTime` datetime DEFAULT NULL COMMENT '存入数据库的时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

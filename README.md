# 使用java自动抓取新浪微博历史列表-免登录

## 目录

[TOC]

## 准备工作

- 安装jdk8
- 安装mysql数据库和navicat
- 高匿代理
- 使用我提供的sql文件和java代码

## 原理

> 找到微博内容页请求链接—》修改参数拼接请求链接—》解析网页数据—》存入数据库

##工作流程

### 1.找出微博内容页请求链接

web端微博加载内容页时，会请求下方箭头指示的链接，点击后可以发现方框中就是整个网页的数据，也就是说我们只需要拿到这个链接就可以解析出网页数据。 

![请求链接](http://img.blog.csdn.net/20180112171143581?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvenN5b3VuZw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)



### 2.修改参数拼出请求链接

  接下来我们需要拼出这个链接，其中需要改动的参数有domain、domain_op、pre_page、_rnd。

 ![链接解析](http://img.blog.csdn.net/20180112171206094?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvenN5b3VuZw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)



在图中我们可以发现id是由domain和一个10位数字拼接出来的，domain和domain_op相同，pre_page指代当前内容页数，_rnd是一个时间戳，cookie我们在运行程序时需要用到。接下来我们讲怎么得到他们：
>  **id**:
>
>- 普通用户的id在微博首页的链接中就可以获取，如：
>
> https://weibo.com/u/2399108715?topnav=1&wvr=6&topsug=1&is_all=1，
>
> 其中`2399108715`就是普通用户id;
>
>- 大V的id需要在他的相册链接中获取，如：
>
> https://weibo.com/p/1006051191044977/hotspot?from=page_100605&mod=TAB#place
>
> 其中`1191044977`就是大V的id;
>
> **domain**：
>
> 下图红框中就是我们需要的`domain`，可以直接在用户微博主页，右键查看源代码看到：
>
> ![domain](http://img.blog.csdn.net/20180112171252044?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvenN5b3VuZw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
>
>  **domain_op**:
>
> 同`domain`；
>
>  **pre_page**:
>
> 我们直接在代码中循环累加`pre_page`即可，但是要知道该用户发出微博的总页数，以终止循环：
>
> ![countPage](http://img.blog.csdn.net/20180112171232790?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvenN5b3VuZw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
>
> 如上图，`countPage`依然可以在用户首页源代码中获取
>
>  **_rnd**:
>
> 一个时间戳，可以在程序中设定为系统当前时间，这样保证可以爬出最新的微博。

### 3.解析网页数据

解析网页数据使用jsoup和正则表达式，具体怎么使用可以下载文末代码去看。

### 4.存入数据库

在程序中实现,使用的是mysql。

## 程序部分

### 程序结构

 ![程序结构](http://img.blog.csdn.net/20180112171315201?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvenN5b3VuZw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

```java
service层：
	build:构建链接
	content:解析网页内容
dao层：
	common:持久化数据，存入数据库
model层：
	SinaModel:实体类
process:
	Main:程序组装
```

### 运行程序部分

修改`process`目录中的`Main`方法中的参数，其中id、cookie上文已经有获取方法，高匿代理的ip和port可以去http://www.xicidaili.com/免费获得。

然后运行`MainTest`程序即可

```java
./test
	./java
		./com
			./yk
				./weibo
					./process
						MainTest
```

### 数据库字段解释

```sql
sina.sql:
	id-表id
	userId-用户Id
	content-微博正文
	contentlink-微博正文中的链接
	imglink-微博中的图片链接
	transmitnum-转发数
	commentnum-评论数
	likenum-喜欢数
	username-用户名
	pulishTime-发布时间
	equipment-设备
	updateTime-数据存入时间
```



##最后再整理一遍思路

> 1.根据用户首页链接:
>
> https://weibo.com/u/2399108715?topnav=1&wvr=6&topsug=1&is_all=1
>
> 获取用户id,domain,domain_op,countPage等参数
>
> 2.拿到参数后，拼接Network中的请求链接：
>
> https://weibo.com/p/aj/v6/mblog/mbloglist?ajwvr=6&domain=100605&from=page_100605&mod=TAB&is_all=1&pagebar=0&pl_name=Pl_Official_MyProfileFeed__23&id=1006051191044977&script_uri=/p/1006051191044977/home&feed_type=0&page=1&pre_page=1&domain_op=100605&__rnd=1515743413572
>
> 这一步一定要注意`is_all`，不要拼成`is_hot`，否则countPage会抓取错误，虽然不是通过这个链接拿到的countPage。
>
> 3.解析请求链接中的网页数据
>
> 4.存入数据库

------

## 文件地址

文件地址：https://gitee.com/zsyoung01/weiboCrawler
博主码云地址：[https://git.oschina.net/zsyoung01](https://git.oschina.net/zsyoung01),欢迎关注！



原文地址：[http://blog.csdn.net/zsyoung/article/details/79046294](http://blog.csdn.net/zsyoung/article/details/79046294)，转载请注明出处！

